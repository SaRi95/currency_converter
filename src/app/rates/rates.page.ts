import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../currency.service';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.page.html',
  styleUrls: ['./rates.page.scss'],
})
export class RatesPage implements OnInit {
  items: any;
  constructor(private currencyService: CurrencyService) { }

  ngOnInit() {
    this.items = this.currencyService.getRates();
  }

}
